sudo echo 'deb http://nebc.nerc.ac.uk/bio-linux/ unstable bio-linux' >> /etc/apt/sources.list
sudo echo 'deb http://ppa.launchpad.net/nebc/bio-linux/ubuntu precise main' >> /etc/apt/sources.list
sudo echo 'deb-src http://ppa.launchpad.net/nebc/bio-linux/ubuntu precise main' >> /etc/apt/sources.list
sudo apt-get -y update --allow-insecure-repositories
sudo apt-get install -y --allow-unauthenticated bio-linux-keyring
sudo apt-get install -y --allow-unauthenticated bio-linux-big-blast